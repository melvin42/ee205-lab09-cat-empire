###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 08a - Cat Wrangler
#
# @file    Makefile
# @version 1.0
#
# @author Melvin Alhambra <melvin42@hawaii.edu>
# @brief  Lab 08a - Cat Wrangler - EE 205 - Spr 2021
# @date   22_Apr_2021
###############################################################################

CXX      = g++
CXXFLAGS = -std=c++20    \
           -O3           \
           -Wall         \
           -pedantic     \
           -Wshadow      \
           -Wconversion

AR       = ar

TARGETS  = familyTree catList catBegat catGenerations

all: $(TARGETS)

cat.o: cat.cpp cat.hpp
	$(CXX) -c $(CXXFLAGS) $<

libcat.a: cat.o
	$(AR) -rsv $@ $^

$(TARGETS): %: %.cpp libcat.a
	$(CXX) $(CXXFLAGS) -o $@ -L. $< -lcat

clean:
	rm -f *.o *.a $(TARGETS)
