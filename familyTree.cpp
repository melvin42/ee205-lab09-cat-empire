///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 09a - Cat Empire!
///
/// @file familyTree.cpp
/// @version 1.0
///
/// Print a family tree of cats
///
/// @author Melvin Alhambra <melvin42@hawaii.edu>
/// @brief  Lab 09a - Cat Empire! - EE 205 - Spr 2021
/// @date   22_Apr_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>

#include "cat.hpp"

using namespace std;

const int NUMBER_OF_CATS = 20;

int main() {
	cout << "Welcome to Cat Empire!" << endl;

	Cat::initNames();

   CatEmpire catEmpire;

	for( int i = 0 ; i < NUMBER_OF_CATS ; i++ ) {
		Cat* newCat = Cat::makeCat();

		catEmpire.addCat( newCat );
	}

	cout << "Print a family tree of " << NUMBER_OF_CATS << " cats" << endl;

	catEmpire.catFamilyTree();

} // main()
